# Typespeed #
This source code was taken from: typespeed.sourceforge.net/typespeed-0.6.5.tar.gz

I keep it here for historical reference.  
My objective is a rewrite in Rust. It can be found here: https://gitlab.com/c64zottel/typespeed-rs


Credit and thanks to the authors of this little funny game!


Build:    
./configure && make all install

Play:  
typespeed 

Also, check out the original README as well as INSTALL file.

typespeed is probably already in your distro's repository.
